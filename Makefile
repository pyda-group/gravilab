BRANCH ?= develop
DOCKER_IMAGE = gwdiexp/gravilab:${BRANCH}
OLD_PY_VERSION = 3.8
NEW_PY_VERSION = 3.10

all: mypy test package

test:
	PYTHONPATH=`pwd` poetry run py.test

test-docker:
	docker run -v `pwd`:/code --rm -it ${DOCKER_IMAGE} make test

black:
	poetry run black gravilab test

package:
	poetry build

upload:
	poetry config pypi-token.pypi ${POETRY_PYPI_TOKEN_PYPI}
	poetry publish

pydocstyle:
	poetry run pydocstyle

pylint:
	poetry run pylint gravilab

docker:
	docker build . -f docker/Dockerfile -t ${DOCKER_IMAGE}
		--build-arg PYTHON_VERSION=${NEW_PY_VERSION}-buster
	docker build . -f docker/Dockerfile -t ${DOCKER_IMAGE}-old \
		--build-arg PYTHON_VERSION=${OLD_PY_VERSION}-buster

docker-push:
	docker login
	docker push ${DOCKER_IMAGE}
	docker push ${DOCKER_IMAGE}-old

clean:
	@rm -r dist/

.PHONY: test package docker
