from pathlib import Path
from unittest import TestCase

import numpy as np
from obspy import UTCDateTime

import gravilab as gl
from gravilab.base.system_element import SystemElement

filedir = Path(__file__).parent


def _max_value_in_window(values, frequencies, input_frequency, window_size=0.01):
    freq_window = np.where(abs(frequencies - input_frequency) < window_size)
    max_value_index = np.argmax(abs(values[freq_window[0][0] : freq_window[0][-1] + 1]))
    max_value_index = max_value_index + freq_window[0][0]
    return values[max_value_index]


class TestGravilab(TestCase):
    def setUp(self):
        root_path = str(filedir)
        # windows shenanigans
        if "\\" in root_path:
            root_path = root_path.replace("\\", "/")
        # go to "data/seismic"
        self.root_path = root_path.replace("/test", "/data/seismic")
        self.s1 = gl.open(
            self.root_path,
            channels={"vertical": "HNZ", "north": "HNY", "east": "HNX"},
            name="seismometer1",
        )
        self.s2 = gl.open(
            self.root_path,
            channels={"vertical": "HNZ", "north": "HNY", "east": "HNX"},
            name="seismometer1",
        )
        self.response = {
            "poles": [
                -0.036614 + 0.037059j,
                -0.036614 - 0.037059j,
                -32.55,
                -142,
                -364 + 404j,
                -364 - 404j,
                -1260,
                -4900 + 5200j,
                -4900 - 5200j,
                -7100 + 1700j,
                -7100 - 1700j,
            ],
            "zeros": [0, 0, -31.63, -160, -350, -3177],
            "scale_factor": 1202.5
            * 8.31871e17
            * 400000,  # V/(m/s) times gain times counts/V
            "omega_exponent": -1,  # convert from m/s to m
        }
        self.s1.response = self.response
        self.s2.response = self.response

    def test_inputs(self):
        self.assertEqual(len(self.s1.time_series.data()), 3)
        self.assertEqual(len(self.s2.time_series.data()), 3)
        for channel in self.s1.time_series.data():
            self.assertEqual(
                self.s1.time_series.data()[channel].stats.starttime,
                UTCDateTime(2021, 5, 5, 0, 0),
            )
            self.assertEqual(
                self.s1.time_series.data()[channel].stats.endtime,
                UTCDateTime(2021, 5, 5, 23, 59, 59, 990000),
            )
        for channel in self.s2.time_series.data():
            self.assertEqual(
                self.s2.time_series.data()[channel].stats.starttime,
                UTCDateTime(2021, 5, 5, 0, 0),
            )
            self.assertEqual(
                self.s2.time_series.data()[channel].stats.endtime,
                UTCDateTime(2021, 5, 5, 23, 59, 59, 990000),
            )

    def test_seismometer_creation(self):
        s0 = SystemElement(self.s1.time_series)
        s1 = gl.Seismometer(self.s1.time_series)
        s2 = gl.Seismometer(spectrum=self.s1.spectrum())
        with self.assertRaises(ValueError):
            s2.time_series
        with self.assertRaises(ValueError):
            s2.spectrum()
        self.assertRaises(ValueError, gl.Seismometer)

    def test_slice_time_series(self):
        starttime = UTCDateTime("2021-05-05T03:00:00")
        endtime = UTCDateTime("2021-05-05T04:00:00")
        s1 = self.s1.slice_time_series(starttime, endtime)
        self.assertEqual(s1.time_series.data()["north"].stats.starttime, starttime)
        self.assertEqual(s1.time_series.data()["north"].stats.endtime, endtime)
        slice1 = s1.time_series.slice(starttime, endtime)
        self.assertEqual(slice1.data()["vertical"].stats.starttime, starttime)
        self.assertEqual(slice1.data()["vertical"].stats.endtime, endtime)

    def test_asd(self):
        starttime = UTCDateTime("2021-05-05T03:00:00")
        endtime = UTCDateTime("2021-05-05T04:00:00")
        s1 = self.s1.slice_time_series(starttime, endtime)
        asd_welch = s1.asd(fftlength=60).data()["east"]
        asd_lpsd = s1.asd(method="lpsd")["east"]
        asd_daniell = s1.asd(method="daniell", number_averages=31)["east"]
        max_asd_welch = _max_value_in_window(
            asd_welch.value, asd_welch.frequencies.value, 0.025
        )
        self.assertAlmostEqual(max_asd_welch, 5.21e-5, delta=1e-7)
        max_asd_lpsd = _max_value_in_window(
            asd_lpsd.value, asd_lpsd.frequencies.value, 0.025
        )
        self.assertAlmostEqual(max_asd_lpsd, 4.39e-5, delta=1e-7)
        max_asd_daniell = _max_value_in_window(
            asd_daniell.value, asd_daniell.frequencies.value, 0.025
        )
        self.assertAlmostEqual(max_asd_daniell, 4.10e-5, delta=1e-7)

    def test_psd(self):
        starttime = UTCDateTime("2021-05-05T03:00:00")
        endtime = UTCDateTime("2021-05-05T04:00:00")
        s1 = self.s1.slice_time_series(starttime, endtime)
        psd_welch = s1.spectrum("psd", fftlength=60).data()["east"]
        with self.assertRaises(ValueError):
            s1.spectrum("unicorns", fftlength=60).data()["east"]
        psd_lpsd = s1.psd(method="lpsd")["east"]
        psd_daniell = s1.psd(method="daniell", number_averages=31)["east"]
        max_psd_welch = _max_value_in_window(
            psd_welch.value, psd_welch.frequencies.value, 0.025
        )
        self.assertAlmostEqual(max_psd_welch, 2.84e-10, delta=1e-12)
        max_psd_lpsd = _max_value_in_window(
            psd_lpsd.value, psd_lpsd.frequencies.value, 0.025
        )
        self.assertAlmostEqual(max_psd_lpsd, 1.93e-10, delta=1e-12)
        max_psd_daniell = _max_value_in_window(
            psd_daniell.value, psd_daniell.frequencies.value, 0.025
        )
        self.assertAlmostEqual(max_psd_daniell, 1.82e-10, delta=1e-12)

    def test_subtract_time_series(self):
        gl.Seismometer(time_series=self.s1.time_series - self.s2.time_series)

    def test_spicypy_time_series(self):
        ts_spicypy = self.s1.time_series.to_spicypy()
        self.assertEqual(len(ts_spicypy["north"]), len(self.s1.time_series["north"]))

    def test_open_one_file(self):
        self.s1 = gl.open(
            self.root_path + "/XX.S0001..HHX_centaur-6_6979_20210505_000000.miniseed",
            channels={"east": "HHX"},
            name="seismometer1",
        )
